===================
QR Code Bookmarklet
===================

What's this?
============
A self-contained webpage
that allow the user to generate QR Codes
within the browser.
We provide a data URI
so it can be bookmarked and used offline.

Usage
=====
Open in a recent Web browser.

Building & Contributing
=======================
Use ``./build test.html``
to build the program uncompressed.
All the source code is in that same file.

To build the production URI,
run ``./build URI``
with `Closure Compiler`_ and `Minify`_ installed.
You might want to use the CI infrastructure
for convenience.

.. _Closure Compiler: https://packages.ubuntu.com/xenial/closure-compiler
.. _Minify: https://packages.debian.org/buster/minify
